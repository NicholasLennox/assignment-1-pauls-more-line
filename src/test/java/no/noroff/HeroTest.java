package no.noroff;

import no.noroff.characters.Hero;
import no.noroff.characters.HeroAttributes;
import no.noroff.characters.Mage;
import no.noroff.exceptions.ArmorException;
import no.noroff.exceptions.WeaponException;
import no.noroff.items.Armor;
import no.noroff.items.Weapon;
import no.noroff.utils.ArmorType;
import no.noroff.utils.Slot;
import no.noroff.utils.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HeroTest {
    @Test
    public void calculateDps_validArmorAndWeaponLevel2Mage_shouldReturnProperDps() throws ArmorException, WeaponException {
        // Ar
        Hero hero = new Mage("John");

        Armor clothRobe = new Armor("Cloth robe", Slot.BODY,
                1,new HeroAttributes(0,0,1),
                ArmorType.CLOTH);
        Weapon standardStaff = new Weapon("Standard Staff", Slot.WEAPON,
                1, 1.1,1.1,
                WeaponType.STAFF);
        double expected = (1.1*1.1)*(1 + (8+5+1)/100.0);
        // Ac
        hero.levelUp();
        hero.equip(clothRobe);
        hero.equip(standardStaff);
        double actual = hero.calculateDps();
        // As
        assertEquals(expected,actual);
    }
}
