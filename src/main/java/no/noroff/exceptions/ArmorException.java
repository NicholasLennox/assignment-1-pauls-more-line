package no.noroff.exceptions;

public class ArmorException extends Exception {
    public ArmorException(String message) {
        super(message);
    }
}
