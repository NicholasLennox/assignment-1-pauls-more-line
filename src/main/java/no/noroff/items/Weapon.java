package no.noroff.items;

import no.noroff.utils.Slot;
import no.noroff.utils.WeaponType;

public class Weapon extends Item {
    private double damage;
    private double attackSpeed;
    private WeaponType weaponType;

    public Weapon(String name, Slot slot, int requiredLevel, double damage, double attackSpeed, WeaponType weaponType) {
        super(name, slot, requiredLevel);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }
    public double getWeaponDps() {
        return damage*attackSpeed;
    }
}
