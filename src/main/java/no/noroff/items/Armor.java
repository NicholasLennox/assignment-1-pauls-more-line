package no.noroff.items;

import no.noroff.characters.HeroAttributes;
import no.noroff.utils.ArmorType;
import no.noroff.utils.Slot;

public class Armor extends Item {
    private HeroAttributes bonusAttributes;
    private ArmorType armorType;

    public Armor(String name, Slot slot, int requiredLevel, HeroAttributes bonusAttributes, ArmorType armorType) {
        super(name, slot, requiredLevel);
        this.bonusAttributes = bonusAttributes;
        this.armorType = armorType;
    }

    public HeroAttributes getBonusAttributes() {
        return bonusAttributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }
}
