package no.noroff;

import no.noroff.characters.Hero;
import no.noroff.characters.HeroAttributes;
import no.noroff.characters.Mage;
import no.noroff.exceptions.ArmorException;
import no.noroff.exceptions.WeaponException;
import no.noroff.items.Armor;
import no.noroff.items.Weapon;
import no.noroff.utils.ArmorType;
import no.noroff.utils.Slot;
import no.noroff.utils.WeaponType;

public class Main {
    public static void main(String[] args) {
        Hero hero = new Mage("John");
        hero.levelUp();
        Armor clothRobe = new Armor("Cloth robe", Slot.BODY,
                1,new HeroAttributes(0,0,1),
                ArmorType.CLOTH);
        Weapon standardStaff = new Weapon("Standard Staff", Slot.WEAPON,
                1, 1.1,1.1,
                WeaponType.STAFF);

        try {
            hero.equip(clothRobe);
        } catch (ArmorException e) {
            e.printStackTrace();
        }

        try {
            hero.equip(standardStaff);
        } catch (WeaponException e) {
            e.printStackTrace();
        }

        System.out.println(hero.calculateDps());
    }
}
