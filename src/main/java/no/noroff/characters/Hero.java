package no.noroff.characters;

import no.noroff.exceptions.ArmorException;
import no.noroff.exceptions.WeaponException;
import no.noroff.items.Armor;
import no.noroff.items.Item;
import no.noroff.items.Weapon;
import no.noroff.utils.ArmorType;
import no.noroff.utils.Slot;
import no.noroff.utils.WeaponType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Hero {
    private int level = 1;
    private String name;
    protected HeroAttributes baseAttributes;
    // Valid armor
    protected List<ArmorType> validArmor;
    // Valid weapon
    protected List<WeaponType> validWeapon;
    // Equipment
    private Map<Slot, Item> equipment = new HashMap<>() {
        {
            put(Slot.BODY, null);
            put(Slot.HEAD, null);
            put(Slot.LEGS, null);
            put(Slot.WEAPON, null);
        }
    };

    public Hero(String name) {
        this.name = name;
    }

    public String levelUp() {
        level++;
        return name + "levelled up, they are now level " + level;
    }

    public String equip(Armor armor) throws ArmorException {
        if(armor.getRequiredLevel() > level)
            throw new ArmorException("Armor level too high");
        if(!validArmor.contains(armor.getArmorType()))
            throw new ArmorException("Cannot equip this armor");
        equipment.put(armor.getSlot(), armor);
        return "New armor equipped";
    }

    public String equip(Weapon weapon) throws WeaponException {
        if(weapon.getRequiredLevel() > level)
            throw new WeaponException("Weapon level too high");
        if(!validWeapon.contains(weapon.getWeaponType()))
            throw new WeaponException("Cannot equip this weapon");
        equipment.put(weapon.getSlot(), weapon);
        return "New weapon equipped";
    }

    private HeroAttributes calculateTotalAttributes() {
        HeroAttributes totalAttributes = new HeroAttributes(0,0,0);
        for (Map.Entry<Slot, Item> entry : equipment.entrySet()) {
            if(entry.getValue() == null)
                continue;
            if(entry.getKey() == Slot.WEAPON)
                continue;
            totalAttributes.add(
                    ((Armor)entry.getValue())
                            .getBonusAttributes());
        }
        totalAttributes.add(baseAttributes);
        return totalAttributes;
    }

    public double calculateDps() {
        HeroAttributes totalAttributes = calculateTotalAttributes();
        int mainAttribute = calculateMainAttributeFromTotal(totalAttributes);
        return getEquippedWeaponDps() * (1 + (mainAttribute/100.0));
    }

    private double getEquippedWeaponDps() {
        double dps = 1;
        if(equipment.get(Slot.WEAPON) != null)
            dps = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDps();
        return dps;
    }

    protected abstract int calculateMainAttributeFromTotal(HeroAttributes totalAttributes);
}
