package no.noroff.characters;

import no.noroff.items.Armor;
import no.noroff.utils.ArmorType;
import no.noroff.utils.WeaponType;

import java.util.ArrayList;
import java.util.Arrays;

public class Mage extends Hero {
    public Mage(String name) {
        super(name);
        baseAttributes = new HeroAttributes(1,1,8);
        validArmor = Arrays.asList(ArmorType.CLOTH);
        validWeapon = Arrays.asList(WeaponType.STAFF,WeaponType.WAND);
    }

    @Override
    public String levelUp() {
        baseAttributes.add(new HeroAttributes(1,1,5));
        return super.levelUp();
    }

    @Override
    protected int calculateMainAttributeFromTotal(HeroAttributes totalAttributes) {
        return totalAttributes.getIntelligence();
    }
}
