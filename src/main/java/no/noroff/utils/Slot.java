package no.noroff.utils;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
