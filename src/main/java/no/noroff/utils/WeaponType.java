package no.noroff.utils;

public enum WeaponType {
    SWORD,
    STAFF,
    WAND
}
