package no.noroff.utils;

public enum ArmorType {
    PLATE,
    CLOTH,
    MAIL,
    LEATHER
}
